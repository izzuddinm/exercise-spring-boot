package com.exercise.exercise.spring.boot.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseDto<T> {

    private List<T> data = new ArrayList<>();
    private String requestId = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
    private String code;
    private String message;
    private Error errors;
    private Date timestamp = new Date();

    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Error {
        private String code;
        private String message;

        public Error() {}

        public Error(String code, String message) {
            this.code = code;
            this.message = message;
        }
    }

    public ResponseDto() {}

    public ResponseDto(List<T> data, String code, String message) {
        this.data = data;
        this.code = code;
        this.message = message;
        this.errors = new Error(code, message);
    }

    public ResponseDto(String message) {
        this.message = message;
    }

    public ResponseDto(List<T> data, String message) {
        this.data = data;
        this.message = message;
    }

    public ResponseDto(List<T> data) {
        this.data = data;
    }

    public ResponseDto(String message, String code) {
        this.errors = new Error(code, message);
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public void addData(T data) {
        this.data.add(data);
    }
}
