package com.exercise.exercise.spring.boot.service;

import org.springframework.http.ResponseEntity;

public interface HealthStatusAppService {

    public ResponseEntity<String> status();

}
