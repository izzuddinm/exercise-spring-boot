package com.exercise.exercise.spring.boot.service;

import com.exercise.exercise.spring.boot.base.ResponseDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public interface HttpService {

    public ResponseEntity<String> getResponseEntity(ResponseDto<?> responseDto);
    public ResponseEntity<String> getResponseEntity(ResponseDto<?> responseDto, HttpStatus httpStatus);
    public ResponseEntity<String> getSuccessResponse(String message);
    public ResponseEntity<String> getErrorResponse(ResponseDto<?> responseDto, HttpStatus httpStatus);

}
