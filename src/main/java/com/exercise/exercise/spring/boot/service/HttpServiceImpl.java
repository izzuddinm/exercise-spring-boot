package com.exercise.exercise.spring.boot.service;

import com.exercise.exercise.spring.boot.base.ResponseDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class HttpServiceImpl implements HttpService {

    private final ObjectMapper objectMapper;

    public HttpServiceImpl(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public ResponseEntity<String> getResponseEntity(ResponseDto<?> responseDto) {
        return new ResponseEntity<>(getDataAsString(responseDto), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> getResponseEntity(ResponseDto<?> responseDto, HttpStatus httpStatus) {
        return new ResponseEntity<>(getDataAsString(responseDto), httpStatus);
    }

    @Override
    public ResponseEntity<String> getSuccessResponse(String message) {
        return new ResponseEntity<>(getDataAsString(new ResponseDto<>(message)), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> getErrorResponse(ResponseDto<?> responseDto, HttpStatus httpStatus) {
        return new ResponseEntity<>(getDataAsString(responseDto), httpStatus);
    }

    private String getDataAsString(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (Exception e) {
            log.error("Error when parsing data: {}", e.getMessage());
            return "{}";
        }
    }
}
