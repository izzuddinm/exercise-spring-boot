package com.exercise.exercise.spring.boot.base;

import com.exercise.exercise.spring.boot.service.HttpService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseService {

    @Autowired
    public HttpService httpService;

}
