package com.exercise.exercise.spring.boot.controller;

import com.exercise.exercise.spring.boot.base.BaseController;
import com.exercise.exercise.spring.boot.service.HealthStatusAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(BaseController.BASE_URL)
public class HealthStatusAppController extends BaseController {

    @Autowired
    private HealthStatusAppService healthStatusAppService;

    @RequestMapping(value = "/application-status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getStatus() {
        return healthStatusAppService.status();
    }
}
