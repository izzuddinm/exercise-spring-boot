package com.exercise.exercise.spring.boot.exception;

import com.exercise.exercise.spring.boot.base.BaseService;
import com.exercise.exercise.spring.boot.base.ResponseDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class GlobalExceptionHandler extends BaseService {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleException(Exception ex, WebRequest request) {
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        String errorMessage = (ex.getCause() != null && ex.getCause().getCause() != null) ? ex.getCause().getCause().toString() : ex.getMessage();
        ResponseDto<String> responseDto = new ResponseDto<>(errorMessage, status.name());
        return httpService.getErrorResponse(responseDto, status);
    }

}

