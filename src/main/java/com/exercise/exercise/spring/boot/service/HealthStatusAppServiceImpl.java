package com.exercise.exercise.spring.boot.service;

import com.exercise.exercise.spring.boot.base.BaseService;
import com.exercise.exercise.spring.boot.base.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HealthStatusAppServiceImpl extends BaseService implements HealthStatusAppService  {

    @Value("${spring.application.name}")
    private String applicationName;

    @Autowired
    private Environment environment;

    @Autowired
    private HttpService httpService;

    @Override
    public ResponseEntity<String> status() {

        Map<String, Object> statusMap = new HashMap<>();
        statusMap.put("status", "Service is running");
        statusMap.put("profile", Arrays.asList(environment.getActiveProfiles()));
        statusMap.put("applicationName", applicationName);

        ResponseDto<Map<String, Object>> response = new ResponseDto<>();
        response.setData(List.of(statusMap));
        return httpService.getResponseEntity(response);
    }
}
